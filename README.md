# WatermarkBot

Don't forget to edit a file `config.py`

```python
botToken = "<token>"
adminChatId = 123456789

chats = {
	123456789: "solinnen.png",
}
```

## Deployment:

```bash
conda env create -f environment.yml
conda activate WatermarkBot
python main.py
```

## Demo:

![](demo1.jpg)
![](demo2.jpg)