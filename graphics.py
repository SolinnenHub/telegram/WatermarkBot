import numpy as np
import imageio
from PIL import Image


class Layer:
	def __init__(self, img, y, x, alpha=1):
		y, x = int(y), int(x)
		self.img = img
		self.y = y
		self.x = x
		self.alpha = alpha

	def getSize(self):
		height = self.img.shape[0]
		width = self.img.shape[1]
		return height, width

	def resize(self, height, width):
		height, width = int(height), int(width)
		self.img = np.array(Image.fromarray(self.img.astype(np.uint8)).resize((width, height))).astype('float32')

class Picture():

	layers = []

	def __init__(self, path):
		self.layers = []
		self.addLayer(path, 0, 0)

	def getSize(self):
		if len(self.layers) == 0:
			raise ValueError("Picture has no layers.")
		return self.layers[0].getSize()

	def render(self, dark=False):
		height, width = self.getSize()

		layers = list(self.layers)
		for i in range(len(layers)):
			layers[i].x = int(layers[i].x)
			layers[i].y = int(layers[i].y)

		for layer in layers[1:]:
			# смещения для слоёв
			layer.img = np.hstack([np.zeros(shape=(layer.img.shape[0], abs(layer.x), layer.img.shape[-1]), dtype=np.uint8), layer.img])
			layer.img = np.vstack([np.zeros(shape=(layer.y, layer.img.shape[1], layer.img.shape[-1]), dtype=np.uint8), layer.img])

			# заполнение оставшегося пространства
			layer.img = np.vstack([layer.img, np.zeros(shape=(abs(height-layer.img.shape[0]), layer.img.shape[1], layer.img.shape[-1]), dtype=np.uint8)])
			layer.img = np.hstack([layer.img, np.zeros(shape=(layer.img.shape[0], abs(width-layer.img.shape[1]), layer.img.shape[-1]), dtype=np.uint8)])
		
			# обрезка лишнего пространства
			layer.img = layer.img[:height, :width, :]

		bg = layers[0].img
		if dark:
			for layer in self.layers[1:]:
				for i in [0,1,2]:
					bg_ = bg[:,:,i] + layer.img[:,:,3] * layer.alpha
					np.clip(bg_, 0, 255, out=bg_)
					bg[:,:,i] = bg_
		else:
			for layer in self.layers[1:]:
				mask = layer.img[:,:,3] / 255
				for i in [0,1,2]:
					bg[:,:,i] = bg[:,:,i] + mask * layer.alpha * (layer.img[:,:,i] - bg[:,:,i])

		return bg[:,:,:3].astype(np.uint8)

	def fixChannels(self, img):
		if len(img.shape) == 2:
			img = np.repeat(img[:, :, np.newaxis], 3, axis=2)
			
		imgChannels = img.shape[-1]
		if imgChannels not in [3, 4]:
			raise ValueError("Unrecognized image format.")

		if imgChannels == 3:
			img = np.insert(
				img,
				imgChannels, # position in the pixel value [ r, g, b, a <-index [3] ]
				255, # or 1 if you're going for a float data type as you want the alpha to be fully white otherwise the entire image will be transparent.
				axis=2, # this is the depth where you are inserting this alpha channel into
			)
		
		return img

	def addLayer(self, path, y, x, alpha=1):
		y, x = int(y), int(x)
		img = imageio.imread(path)
		img = self.fixChannels(img)
		layer = Layer(img, y, x, alpha)
		self.layers.append(layer)

	def save(self, path, mime, dark=False):
		img = self.render(dark)
		imageio.imwrite(path, img, format=mime)