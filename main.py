import asyncio
import os, shutil
import time
from aiogram import Bot, types
from aiogram.dispatcher import Dispatcher
from aiogram.types.message import ContentType
from aiogram.contrib.middlewares.logging import LoggingMiddleware
from aiogram.utils import exceptions as aioexceptions

from utils import print

from modules.chat import ChatManager
import config as cfg

class Main:

	def __init__(self):
		while True:
			try:
				asyncio.run(self.main())
			except KeyboardInterrupt:
				return
			except Exception as e:
				time.sleep(30)
				print(f"ERROR: {e}")

	async def main(self):
		self.bot = Bot(token=cfg.botToken)
		dp = Dispatcher(self.bot)
		dp.middleware.setup(LoggingMiddleware())

		self.chatManager = ChatManager(self)

		await self.register_handlers(dp)

		shutil.rmtree("cache")
		os.mkdir("cache")

		print("The bot has been launched.")
		await dp.start_polling(allowed_updates=types.AllowedUpdates.all())

	async def register_handlers(self, dp):
		dp.register_channel_post_handler(self.chatManager.onPhoto, content_types=ContentType.PHOTO)
		dp.register_message_handler(self.chatManager.onPhoto, content_types=ContentType.PHOTO)

		dp.register_errors_handler(self.chatManager.ignore, exception=aioexceptions.TelegramAPIError)
		dp.register_errors_handler(self.chatManager.onException, exception=None)

Main()