import os
from uuid import uuid4
from aiogram import types
from aiogram.types import ParseMode

from modules.photo import processPhoto
from utils import Utils, print
import config as cfg

class ChatManager:

	def __init__(self, parent_inst):
		self.inst = parent_inst
		self.media_group_storage = {}

	async def onPhoto(self, message: types.Message):
		if message.chat.id not in cfg.chats and message.chat.id != cfg.adminChatId:
			return

		mgid = message.media_group_id
		if mgid:
			if self.media_group_storage.get(mgid, None):
				self.media_group_storage[mgid]['open'] += 1
			else:
				self.media_group_storage[mgid] = {
					'open': 1,
					'items': [],
					'chatId': message.chat.id,
					'replyToId': message.message_id
				}

		file_id = message.photo[-1].file_id # [-1] - наивысшее разрешение
		file_size = message.photo[-1].file_size
		human_file_size_mb = round(file_size/1024**2*100)/100

		try:
			path = f'cache/{uuid4()}.jpg'
			print(f' + {path} ({human_file_size_mb}mb) PHOTO')
			await self.inst.bot.download_file_by_id(file_id, path)
			path = processPhoto(path, cfg.chats[message.chat.id])

			if mgid:
				self.media_group_storage[mgid]['open'] -= 1
				self.media_group_storage[mgid]['items'].append({
					'path': path,
					'message_id': message.message_id
				})
				if self.media_group_storage[mgid]['open'] == 0:
					await self.sendFromMediaGroupStorage(mgid)
					await message.delete()
			else:
				await message.reply_photo(photo=open(path, 'rb'), reply=False)
				await message.delete()
				print(f' - {path} ({human_file_size_mb}mb) PHOTO')
				os.remove(path)
		except Exception as e:
			print(f"WARNING: {e}")
			raise e

	async def sendFromMediaGroupStorage(self, mgid):
		group = self.media_group_storage[mgid]
		group['items'].sort(key=lambda x: x['message_id'])
		
		media = types.MediaGroup()
		for item in group['items']:
			media.attach_photo(types.InputFile(item['path']))
		messages = await self.inst.bot.send_media_group(group['chatId'], media, reply_to_message_id=group['replyToId'])

		for item in group['items']:
			os.remove(item['path'])
			print(f' - {item["path"]} PHOTO')
		del self.media_group_storage[mgid]


	async def ignore(self, update: types.Update, exception):
		return True

	async def onException(self, event, exception):
		text = f'```\n{exception}\n\n{event}```'
		await self.inst.bot.send_message(cfg.adminChatId, text, parse_mode=ParseMode.MARKDOWN)
		print(f"Error during event: {event}")
		raise exception