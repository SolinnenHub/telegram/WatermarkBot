import numpy as np
from graphics import Picture

def processPhoto(path, watermark_file_name):
	pic = Picture(path)
	height, width = pic.getSize()

	pic.addLayer('images/'+watermark_file_name, 0, 0, alpha=0.3)
	picHeight, picWidth = pic.layers[-1].getSize()

	newPicWidth = max(80, min(width*0.3, 180))
	newPicHeight = picHeight * newPicWidth/picWidth
	pic.layers[-1].resize(newPicHeight, newPicWidth)

	pic.layers[-1].x = (width - newPicWidth) / 2
	pic.layers[-1].y = (height - newPicHeight) / 2

	fragment = pic.layers[0].img[
		int(pic.layers[-1].y):int(pic.layers[-1].y+newPicHeight),
		int(pic.layers[-1].x):int(pic.layers[-1].x+newPicWidth)
	]

	dark = np.mean(fragment) / 255 < 0.5
	pic.save(path, 'png', dark=dark)
	return path