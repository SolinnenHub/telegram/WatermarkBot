#!/bin/bash

ARGS=("$@")
SESSION=${PWD##*/} # tmux session name = directory name

if ! [[ $(tmux ls | grep $SESSION) ]]; then
	tmux new-session -d -s $SESSION

	tmux send-keys 'conda activate ' $SESSION C-m # virtual environment name = tmux session name
	tmux send-keys 'python main.py' C-m
fi

# if executed with the -d argument, then connection to the session will not occur
if ! [[ $ARGS = "-d" || $ARGS = "--daemon" ]]; then
	tmux attach-session -t $SESSION
fi