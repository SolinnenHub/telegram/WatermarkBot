import sys
import time
from datetime import datetime

class Utils:

	@staticmethod
	def time():
		ts = int(time.time())
		return datetime.utcfromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

def print(obj):
	sys.stdout.write(f"{Utils.time()} {obj}\n")
	sys.stdout.flush()